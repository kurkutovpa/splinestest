﻿using SharpGL.SceneGraph;
using SharpGL.SceneGraph.Core;
using SplineTest.Math;
using System.Xml.Serialization;

namespace SharpGL.Extensions
{
    /// <summary>
    /// Используется в 3D-сцене для отображения набора интерполированных данных.
    /// </summary>
    public class DataGraph : SceneElement, IRenderable
    {
        [XmlIgnore]
        private DisplayList displayList;

        private SplineFragment[] sourceData;

        public DataGraph(SplineFragment[] sourceData)
        {
            Name = "DataGraph";

            UpdateData(sourceData);
        }

        public void Render(OpenGL gl, RenderMode renderMode)
        {
            if (renderMode != RenderMode.Design)
                return;

            if (displayList == null)
                CreateDisplayList(gl);
            else
                displayList.Call(gl);

            foreach (var child in Children)
            {
                var renderChild = child as IRenderable;
                renderChild.Render(gl, renderMode);
            }
        }

        private void CreateDisplayList(OpenGL gl)
        {
            // подготавливаем список для отображения
            displayList = new DisplayList();
            displayList.Generate(gl);
            displayList.New(gl, DisplayList.DisplayListMode.CompileAndExecute);

            // настраиваем экземпляр OpenGL для отображения
            gl.PushAttrib(OpenGL.GL_CURRENT_BIT | OpenGL.GL_ENABLE_BIT | OpenGL.GL_LINE_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_TEXTURE_2D);
            gl.DepthFunc(OpenGL.GL_ALWAYS);

            // отображаем исходные данные
            gl.Color(0.0f, 0.4f, 0.2f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            for (var i = 0; i < sourceData.Length; i++)
            {
                var fragmentData = sourceData[i].Data;
                var pointPos = new Vertex(fragmentData.X, fragmentData.Y, fragmentData.Z);

                gl.Vertex(pointPos);

                AddChild(new GraphPoint(pointPos));
            }
            gl.End();

            // отображаем интерполированные данные.
            gl.Color(1f, 1f, 1f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            for (var i = 0; i < sourceData.Length; i++)
            {
                var fragment = sourceData[i];
                if (fragment.Points == null)
                    continue;

                for (var j = 0; j < fragment.Points.Length; j++)
                {
                    var point = fragment.Points[j];
                    gl.Vertex(point.X, point.Y, point.Z);
                }

            }
            gl.End();

            // завершаем отрисовку
            gl.PopAttrib();
            displayList.End(gl);
        }

        /// <summary>
        /// Обновленяет измененные данные.
        /// </summary>
        /// <param name="sourceData">Измененные данные.</param>
        /// <remarks>
        /// Этот метод необходимо вызывать каждый раз, когда произошло изменение данных сплайна.
        /// Метод перерисовывает графическое представление сплайна.
        /// </remarks>
        public void UpdateData(SplineFragment[] sourceData)
        {
            this.sourceData = sourceData;

            displayList = null;
            Children.Clear();
        }
    }
}
