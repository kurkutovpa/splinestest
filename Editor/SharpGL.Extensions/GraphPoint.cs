﻿using SharpGL.SceneGraph;
using SharpGL.SceneGraph.Primitives;

namespace SharpGL.Extensions
{

    /// <summary>
    /// Изображает управляющую точку на графе.
    /// </summary>
    public class GraphPoint : Polygon
    {
        private Vertex position;

        public Vertex Position { get { return position; } }

        public GraphPoint(Vertex position)
        {
            this.position = position;

            Name = "GraphPoint";

            CreateGeometry(position);
        }

        private void CreateGeometry(Vertex position)
        {
            UVs.Add(new UV(0, 0));
            UVs.Add(new UV(0, 1));
            UVs.Add(new UV(1, 1));
            UVs.Add(new UV(1, 0));

            //	Add the vertices.
            Vertices.Add(position + new Vertex(-1, -1, -1) * 0.01f);
            Vertices.Add(position + new Vertex(1, -1, -1) * 0.01f);
            Vertices.Add(position + new Vertex(1, -1, 1) * 0.01f);
            Vertices.Add(position + new Vertex(-1, -1, 1) * 0.01f);
            Vertices.Add(position + new Vertex(-1, 1, -1) * 0.01f);
            Vertices.Add(position + new Vertex(1, 1, -1) * 0.01f);
            Vertices.Add(position + new Vertex(1, 1, 1) * 0.01f);
            Vertices.Add(position + new Vertex(-1, 1, 1) * 0.01f);
        }
    }
}
