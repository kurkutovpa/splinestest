﻿namespace SplineTest.Editor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.sceneControl = new SharpGL.SceneControl();
            this.toolPanel = new System.Windows.Forms.Panel();
            this.fragmentListBox = new System.Windows.Forms.ListBox();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.addPointToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.removeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.panelFragmentDetails = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonApplyDetails = new System.Windows.Forms.Button();
            this.textBoxSplineB = new System.Windows.Forms.TextBox();
            this.textBoxSplineC = new System.Windows.Forms.TextBox();
            this.textBoxSplineT = new System.Windows.Forms.TextBox();
            this.textBoxPositionZ = new System.Windows.Forms.TextBox();
            this.textBoxPositionY = new System.Windows.Forms.TextBox();
            this.textBoxPositionX = new System.Windows.Forms.TextBox();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.sceneControl)).BeginInit();
            this.toolPanel.SuspendLayout();
            this.mainToolStrip.SuspendLayout();
            this.panelFragmentDetails.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // sceneControl
            // 
            this.sceneControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sceneControl.DrawFPS = false;
            this.sceneControl.Location = new System.Drawing.Point(260, 24);
            this.sceneControl.Name = "sceneControl";
            this.sceneControl.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_1;
            this.sceneControl.RenderContextType = SharpGL.RenderContextType.FBO;
            this.sceneControl.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.sceneControl.Size = new System.Drawing.Size(621, 479);
            this.sceneControl.TabIndex = 5;
            this.sceneControl.OpenGLInitialized += new System.EventHandler(this.sceneControl_OpenGLInitialized);
            this.sceneControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.sceneControl_KeyUp);
            this.sceneControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sceneControl_MouseDown);
            this.sceneControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.sceneControl_MouseMove);
            this.sceneControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.sceneControl_MouseUp);
            // 
            // toolPanel
            // 
            this.toolPanel.Controls.Add(this.fragmentListBox);
            this.toolPanel.Controls.Add(this.mainToolStrip);
            this.toolPanel.Controls.Add(this.panelFragmentDetails);
            this.toolPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolPanel.Location = new System.Drawing.Point(0, 24);
            this.toolPanel.Name = "toolPanel";
            this.toolPanel.Size = new System.Drawing.Size(260, 479);
            this.toolPanel.TabIndex = 4;
            // 
            // fragmentListBox
            // 
            this.fragmentListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fragmentListBox.FormattingEnabled = true;
            this.fragmentListBox.Location = new System.Drawing.Point(0, 25);
            this.fragmentListBox.Name = "fragmentListBox";
            this.fragmentListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.fragmentListBox.Size = new System.Drawing.Size(260, 363);
            this.fragmentListBox.TabIndex = 8;
            this.fragmentListBox.SelectedIndexChanged += new System.EventHandler(this.fragmentListBox_SelectedValueChanged);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPointToolStripButton,
            this.removeToolStripButton});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(260, 25);
            this.mainToolStrip.TabIndex = 7;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // addPointToolStripButton
            // 
            this.addPointToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addPointToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addPointToolStripButton.Image")));
            this.addPointToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addPointToolStripButton.Name = "addPointToolStripButton";
            this.addPointToolStripButton.Size = new System.Drawing.Size(64, 22);
            this.addPointToolStripButton.Text = "Add point";
            this.addPointToolStripButton.Click += new System.EventHandler(this.addPointToolStripButton_Click);
            // 
            // removeToolStripButton
            // 
            this.removeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.removeToolStripButton.Enabled = false;
            this.removeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("removeToolStripButton.Image")));
            this.removeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeToolStripButton.Name = "removeToolStripButton";
            this.removeToolStripButton.Size = new System.Drawing.Size(90, 22);
            this.removeToolStripButton.Text = "Remove points";
            this.removeToolStripButton.Click += new System.EventHandler(this.removeToolStripButton_Click);
            // 
            // panelFragmentDetails
            // 
            this.panelFragmentDetails.Controls.Add(this.label6);
            this.panelFragmentDetails.Controls.Add(this.label5);
            this.panelFragmentDetails.Controls.Add(this.label4);
            this.panelFragmentDetails.Controls.Add(this.label3);
            this.panelFragmentDetails.Controls.Add(this.label2);
            this.panelFragmentDetails.Controls.Add(this.label1);
            this.panelFragmentDetails.Controls.Add(this.buttonApplyDetails);
            this.panelFragmentDetails.Controls.Add(this.textBoxSplineB);
            this.panelFragmentDetails.Controls.Add(this.textBoxSplineC);
            this.panelFragmentDetails.Controls.Add(this.textBoxSplineT);
            this.panelFragmentDetails.Controls.Add(this.textBoxPositionZ);
            this.panelFragmentDetails.Controls.Add(this.textBoxPositionY);
            this.panelFragmentDetails.Controls.Add(this.textBoxPositionX);
            this.panelFragmentDetails.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFragmentDetails.Location = new System.Drawing.Point(0, 388);
            this.panelFragmentDetails.Name = "panelFragmentDetails";
            this.panelFragmentDetails.Size = new System.Drawing.Size(260, 91);
            this.panelFragmentDetails.TabIndex = 3;
            this.panelFragmentDetails.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(169, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "B:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(89, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "C:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "T:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Z:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "X:";
            // 
            // buttonApplyDetails
            // 
            this.buttonApplyDetails.Location = new System.Drawing.Point(5, 58);
            this.buttonApplyDetails.Name = "buttonApplyDetails";
            this.buttonApplyDetails.Size = new System.Drawing.Size(75, 23);
            this.buttonApplyDetails.TabIndex = 6;
            this.buttonApplyDetails.Text = "apply";
            this.buttonApplyDetails.UseVisualStyleBackColor = true;
            this.buttonApplyDetails.Click += new System.EventHandler(this.buttonApplyDetails_Click);
            // 
            // textBoxSplineB
            // 
            this.textBoxSplineB.Location = new System.Drawing.Point(192, 32);
            this.textBoxSplineB.Name = "textBoxSplineB";
            this.textBoxSplineB.Size = new System.Drawing.Size(51, 20);
            this.textBoxSplineB.TabIndex = 5;
            // 
            // textBoxSplineC
            // 
            this.textBoxSplineC.Location = new System.Drawing.Point(112, 32);
            this.textBoxSplineC.Name = "textBoxSplineC";
            this.textBoxSplineC.Size = new System.Drawing.Size(51, 20);
            this.textBoxSplineC.TabIndex = 4;
            // 
            // textBoxSplineT
            // 
            this.textBoxSplineT.Location = new System.Drawing.Point(32, 32);
            this.textBoxSplineT.Name = "textBoxSplineT";
            this.textBoxSplineT.Size = new System.Drawing.Size(51, 20);
            this.textBoxSplineT.TabIndex = 3;
            // 
            // textBoxPositionZ
            // 
            this.textBoxPositionZ.Location = new System.Drawing.Point(192, 6);
            this.textBoxPositionZ.Name = "textBoxPositionZ";
            this.textBoxPositionZ.Size = new System.Drawing.Size(51, 20);
            this.textBoxPositionZ.TabIndex = 2;
            // 
            // textBoxPositionY
            // 
            this.textBoxPositionY.Location = new System.Drawing.Point(112, 6);
            this.textBoxPositionY.Name = "textBoxPositionY";
            this.textBoxPositionY.Size = new System.Drawing.Size(51, 20);
            this.textBoxPositionY.TabIndex = 1;
            // 
            // textBoxPositionX
            // 
            this.textBoxPositionX.Location = new System.Drawing.Point(32, 6);
            this.textBoxPositionX.Name = "textBoxPositionX";
            this.textBoxPositionX.Size = new System.Drawing.Size(51, 20);
            this.textBoxPositionX.TabIndex = 0;
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(881, 24);
            this.menuStripMain.TabIndex = 6;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator2,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(109, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.saveToolStripMenuItem.Text = "Save...";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(109, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "tcbs";
            this.saveFileDialog.Filter = "TCB-spline|*.tcbs";
            this.saveFileDialog.RestoreDirectory = true;
            this.saveFileDialog.Title = "Save TCB-splines";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "tcbs";
            this.openFileDialog.Filter = "TCP-splines|*tcbs";
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Open TCB-splines";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 503);
            this.Controls.Add(this.sceneControl);
            this.Controls.Add(this.toolPanel);
            this.Controls.Add(this.menuStripMain);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "MainForm";
            this.Text = "SharpGL Form";
            ((System.ComponentModel.ISupportInitialize)(this.sceneControl)).EndInit();
            this.toolPanel.ResumeLayout(false);
            this.toolPanel.PerformLayout();
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.panelFragmentDetails.ResumeLayout(false);
            this.panelFragmentDetails.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SharpGL.SceneControl sceneControl;
        private System.Windows.Forms.Panel toolPanel;
        private System.Windows.Forms.Panel panelFragmentDetails;
        private System.Windows.Forms.Button buttonApplyDetails;
        private System.Windows.Forms.TextBox textBoxSplineB;
        private System.Windows.Forms.TextBox textBoxSplineC;
        private System.Windows.Forms.TextBox textBoxSplineT;
        private System.Windows.Forms.TextBox textBoxPositionZ;
        private System.Windows.Forms.TextBox textBoxPositionY;
        private System.Windows.Forms.TextBox textBoxPositionX;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ListBox fragmentListBox;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton addPointToolStripButton;
        private System.Windows.Forms.ToolStripButton removeToolStripButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}

