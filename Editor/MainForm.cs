﻿using SharpGL.Extensions;
using SharpGL.SceneGraph;
using SharpGL.SceneGraph.Cameras;
using SharpGL.SceneGraph.Primitives;
using SplineTest.Editor.Commands;
using SplineTest.Math;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace SplineTest.Editor
{
    public partial class MainForm : Form
    {
        private readonly Spline spline = new Spline();

        private ArcBallCamera camera = new ArcBallCamera();
        private DataGraph dataGraph;

        // Stack<T> не подходит из-за сложности реализации стека фиксированной длины
        private const int historyLength = 10;
        private readonly List<Command> history = new List<Command>(historyLength);
        private int? historyIndex;

        private EditorMode mode;
        private Point dragStartLocation;
        private SplineFragment dragFragment;

        public MainForm()
        {
            Application.CurrentCulture = new CultureInfo("en-US");

            InitializeComponent();

            AddFragment(new Vertex(-1f, 1, 0));
            AddFragment(new Vertex(0, 0, 0));
            AddFragment(new Vertex(1f, 1, 0.7f));
            AddFragment(new Vertex(1.5f, -3.5f, 2));

            UpdateHistoryGUI();
        }

        /// <summary>
        /// Добавление нового фрагмента сплайна в конец кривой.
        /// </summary>
        /// <param name="position">Координаты новой управляющей точки.</param>
        /// <returns></returns>
        public SplineFragment AddFragment(Vertex position)
        {
            var newFragment = new SplineFragment
            {
                Data = new Vector3D(position.X, position.Y, position.Z),
            };

            AddFragment(newFragment);

            return newFragment;
        }

        /// <summary>
        /// Добавление нового фрагмента сплайна в конец кривой.
        /// </summary>
        /// <param name="fragment">Новый фрагмент сплайна.</param>
        public void AddFragment(SplineFragment fragment)
        {
            spline.Fragments.Add(fragment);
            spline.RecalculatePoint(spline.Fragments.Count - 1);
            UpdateFragmentGUI();
        }

        /// <summary>
        /// Удаление указанных фрагментов сплайна.
        /// </summary>
        /// <param name="fragments">Фрагменты, подлежащие удалению.</param>
        public void RemoveFragments(IEnumerable<SplineFragment> fragments)
        {
            foreach (var fragment in fragments)
            {
                spline.EriseInterpolation(fragment);
                spline.Fragments.Remove(fragment);
            }

            spline.Recalculate();

            UpdateFragmentGUI();
            UpdateMainToolBar();
        }

        /// <summary>
        /// Редактирование данных указанных фрагментов сплайна.
        /// </summary>
        /// <param name="fragments">Редактируемые фрагменты.</param>
        /// <param name="x">Х координата управлябщей точки.</param>
        /// <param name="y">Y координата управлябщей точки.</param>
        /// <param name="z">Z координата управлябщей точки.</param>
        /// <param name="t">Tension значение управлябщей точки.</param>
        /// <param name="c">Continuity значение управлябщей точки.</param>
        /// <param name="b">Bias значение управлябщей точки.</param>
        /// <remarks>
        /// Значения, указанные как null, не будут изменяться в выбранных фрагментах.
        /// </remarks>
        public void EditFragment(IEnumerable<SplineFragment> fragments,
            float? x, float? y, float? z,
            float? t, float? c, float? b)
        {
            foreach (var fragment in fragments)
            {
                if (x != null || y != null || z != null)
                {
                    var newData = new Vector3D
                    {
                        X = x != null ? x.Value : fragment.Data.X,
                        Y = y != null ? y.Value : fragment.Data.Y,
                        Z = z != null ? z.Value : fragment.Data.Z
                    };
                    fragment.Data = newData;
                }

                if (t != null)
                    fragment.Tension = t.Value;

                if (c != null)
                    fragment.Continuity = c.Value;

                if (b != null)
                    fragment.Bias = b.Value;

                spline.EriseInterpolation(fragment);
            }
            spline.Recalculate();

            UpdateFragmentGUI();
        }

        /// <summary>
        /// Перемещение указанных фрагментов в пространстве.
        /// </summary>
        /// <param name="fragments">Перемещаемые фрагменты.</param>
        /// <param name="translation">Вектор, на который будут перемещены управляющие точки.</param>
        /// <remarks>Все указанные управляющие точки будут перемещены на translation.</remarks>
        public void TranslateFragments(IEnumerable<SplineFragment> fragments, Vertex translation)
        {
            foreach (var fragment in fragments)
            {
                fragment.Data += new Vector3D(translation.X, translation.Y, translation.Z);
                spline.EriseInterpolation(fragment);
            }
            spline.Recalculate();

            UpdateFragmentGUI();
        }



        private void sceneControl_OpenGLInitialized(object sender, EventArgs e)
        {
            var scene = sceneControl.Scene;
            sceneControl.Scene.SceneContainer.Children.Clear();
            sceneControl.Scene.SceneContainer.AddChild(new Axies());

            camera.AspectRatio = (float)sceneControl.Width / (float)sceneControl.Height;
            camera.Near = 0.01f;
            camera.Far = 100f;
            camera.FieldOfView = 60;
            camera.Position = new Vertex(-10, -10, 15);
            sceneControl.Scene.CurrentCamera = camera;

            dataGraph = new DataGraph(spline.Fragments.ToArray());
            sceneControl.Scene.SceneContainer.AddChild(dataGraph);
        }

        private void sceneControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (mode == EditorMode.Drag)
                {
                    var gl = sceneControl.OpenGL;
                    var fragmentPosition = new Vertex(dragFragment.Data.X, dragFragment.Data.Y, dragFragment.Data.Z);

                    var dragNearStartCoords = gl.UnProject(dragStartLocation.X, sceneControl.Height - dragStartLocation.Y, 0);
                    var dragNearStartPosition = new Vertex((float)dragNearStartCoords[0], (float)dragNearStartCoords[1], (float)dragNearStartCoords[2]);

                    var distanceToFragment = (float)(dragNearStartPosition - fragmentPosition).Magnitude();

                    var invertedDragStartPoint = new Point(dragStartLocation.X, sceneControl.Height - dragStartLocation.Y);
                    var dragStartPosition = ScreenToWorld(invertedDragStartPoint, distanceToFragment);

                    var invertedDragFinishPoint = new Point(e.X, sceneControl.Height - e.Y);
                    var dragFinishPosition = ScreenToWorld(invertedDragFinishPoint, distanceToFragment);

                    var translation = dragFinishPosition - dragStartPosition;
                    var fragments = fragmentListBox.SelectedItems.Cast<SplineFragment>();
                    var command = new TranslatePointsCommand(this, fragments, translation);
                    command.Execute();
                    WriteHistory(command);

                    mode = EditorMode.Selection;

                    TCBSplines.Interpolate(spline.Fragments.ToArray());
                    UpdateFragmentGUI();
                    sceneControl.Cursor = Cursors.Default;
                }
            }

            if (e.Button == MouseButtons.Middle)
            {
                camera.ArcBall.MouseUp(e.X, e.Y);
            }
        }

        private void sceneControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (mode == EditorMode.Drag)
                {

                }
            }

            if (e.Button == MouseButtons.Middle)
            {
                camera.ArcBall.MouseMove(e.X, e.Y);
            }
        }

        private void sceneControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (mode == EditorMode.Creation)
                {
                    var invertedPoint = new Point(e.X, sceneControl.Height - e.Y);
                    var modelWorldPoint = ScreenToWorld(invertedPoint, (float)camera.Position.Magnitude());

                    var command = new AddPointCommand(this, modelWorldPoint);
                    command.Execute();
                    WriteHistory(command);

                    mode = EditorMode.Selection;
                    UpdateMainToolBar();
                    sceneControl.Cursor = Cursors.Default;
                }
                else
                {
                    var selectedObjects = sceneControl.Scene.DoHitTest(e.X, e.Y);

                    var selectedFragment = (from fragment in spline.Fragments
                                            from obj in selectedObjects
                                            let gPoint = obj as GraphPoint
                                            where gPoint != null
                                            where fragment.Data.X == gPoint.Position.X
                                            where fragment.Data.Y == gPoint.Position.Y
                                            where fragment.Data.Z == gPoint.Position.Z
                                            select fragment).FirstOrDefault();

                    if (Control.ModifierKeys == Keys.Shift)
                    {
                        fragmentListBox.SelectedItems.Add(selectedFragment);
                    }
                    else
                    {
                        // начинаем перетаскивание точек, если кликнули на уже выбранную
                        var fragmentInList = (from fragment in fragmentListBox.SelectedItems.Cast<SplineFragment>()
                                              where fragment == selectedFragment
                                              select fragment).SingleOrDefault();

                        if (fragmentInList != null)
                        {
                            mode = EditorMode.Drag;
                            dragStartLocation = e.Location;
                            dragFragment = fragmentInList;
                            sceneControl.Cursor = Cursors.NoMove2D;
                        }
                        else
                        {
                            fragmentListBox.SelectedItems.Clear();
                            fragmentListBox.SelectedItem = selectedFragment;
                        }
                    }
                }
            }

            if (e.Button == MouseButtons.Middle)
            {
                camera.ArcBall.SetBounds(sceneControl.Width, sceneControl.Height);
                camera.ArcBall.MouseDown(e.X, e.Y);
            }
        }

        private void sceneControl_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.A:
                    addPointToolStripButton.PerformClick();
                    break;

                case Keys.Z:
                    if (e.Control)
                        undoToolStripMenuItem.PerformClick();
                    break;

                case Keys.Y:
                    if (e.Control)
                        redoToolStripMenuItem.PerformClick();
                    break;

                case Keys.Escape:
                    mode = EditorMode.Selection;
                    sceneControl.Cursor = Cursors.Default;
                    UpdateMainToolBar();
                    break;
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            spline.Fragments.Clear();
            history.Clear();
            UpdateFragmentGUI();
            UpdateMainToolBar();
        }

        private void addPointToolStripButton_Click(object sender, EventArgs e)
        {
            if (mode == EditorMode.Creation)
            {
                mode = EditorMode.Selection;
            }
            else
            {
                mode = EditorMode.Creation;
                sceneControl.Cursor = Cursors.Cross;
                sceneControl.Focus();
            }

            UpdateMainToolBar();
        }

        private void removeToolStripButton_Click(object sender, EventArgs e)
        {
            var selectedFragments = fragmentListBox.SelectedItems.Cast<SplineFragment>();

            var command = new RemovePointsCommand(this, selectedFragments);
            command.Execute();
            WriteHistory(command);
        }

        private void buttonApplyDetails_Click(object sender, EventArgs e)
        {
            var selectedFragments = fragmentListBox.SelectedItems.Cast<SplineFragment>();

            try
            {
                var x = ParseFragmentGUIValue(textBoxPositionX.Text);
                var y = ParseFragmentGUIValue(textBoxPositionY.Text);
                var z = ParseFragmentGUIValue(textBoxPositionZ.Text);

                var tension = ParseFragmentGUIValue(textBoxSplineT.Text);
                var continuity = ParseFragmentGUIValue(textBoxSplineC.Text);
                var bias = ParseFragmentGUIValue(textBoxSplineB.Text);

                var command = new EditPointsCommand(this, selectedFragments, x, y, z, tension, continuity, bias);
                command.Execute();
                WriteHistory(command);
            }
            catch (FormatException exception)
            {
                MessageBox.Show("Invalid format.\n" + exception.ToString());
            }
        }

        private void fragmentListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedFragment = fragmentListBox.SelectedItems.Cast<SplineFragment>();
            UpdateFragmentGUIDetails(selectedFragment);
            UpdateMainToolBar();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var formatter = new BinaryFormatter();
                using (var stream = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    formatter.Serialize(stream, spline.Fragments.ToArray());
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var stream = File.OpenRead(openFileDialog.FileName))
                {
                    var formatter = new BinaryFormatter();

                    var fragmentArray = (SplineFragment[])formatter.Deserialize(stream);
                    spline.Fragments.Clear();
                    spline.Fragments.AddRange(fragmentArray);
                    dataGraph.UpdateData(fragmentArray);

                    UpdateFragmentGUI();
                }
            }

            historyIndex = null;
            history.Clear();
            UpdateHistoryGUI();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UndoHistory();
            UpdateHistoryGUI();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RedoHistory();
            UpdateHistoryGUI();
        }


        /// <summary>
        /// Запись в историю текущей команды.
        /// </summary>
        /// <param name="command">Команда для записи в историю.</param>
        /// <remarks>
        /// Команда будет записана либо в конец истории либо перепишет остаток истории. Остатком истории называются команды, откатанные операцией undo.
        /// </remarks>
        private void WriteHistory(Command command)
        {
            // переписываем историю с текущего момента
            if (historyIndex != null)
            {
                if (historyIndex < history.Count - 1)
                    history.RemoveRange(historyIndex.Value + 1, history.Count - (historyIndex.Value + 1));

                historyIndex = null;
            }

            // запоминаем последнее выполненое действие
            if (history.Count > historyLength)
                history.RemoveAt(0);

            history.Add(command);
            UpdateHistoryGUI();
        }

        /// <summary>
        /// Откат истории на пункт назад.
        /// </summary>
        private void UndoHistory()
        {
            if (history.Count == 0)
                return;

            if (historyIndex == null)
                historyIndex = history.Count - 1;

            history[historyIndex.Value].Revert();
            historyIndex--;

            UpdateHistoryGUI();
        }

        /// <summary>
        /// Откат истории на пункт вперед.
        /// </summary>
        private void RedoHistory()
        {
            if (history.Count == 0)
                return;

            if (historyIndex < history.Count - 1)
            {
                // здесь не проверяем индекс на null, 
                // потому что redo не должен выполняться, если не выполнялось undo.
                // Undo обязательно запишет значение в historyIndex.
                historyIndex++;

                history[historyIndex.Value].Execute();
            }

            UpdateHistoryGUI();
        }


        private void UpdateFragmentGUI()
        {
            var selectedFragments = fragmentListBox.SelectedItems.Cast<SplineFragment>().ToArray();

            dataGraph.UpdateData(spline.Fragments.ToArray());

            fragmentListBox.Items.Clear();
            for (var i = 0; i < spline.Fragments.Count; i++)
                fragmentListBox.Items.Add(spline.Fragments[i]);

            fragmentListBox.SelectedItems.Clear();
            foreach (var fragment in selectedFragments)
                fragmentListBox.SelectedItems.Add(fragment);
        }

        private void UpdateFragmentGUIDetails(IEnumerable<SplineFragment> fragments)
        {
            if (fragments != null && fragments.Any())
            {
                textBoxPositionX.Text = GetFragmentsGUIValue(fragments, x => x.Data.X);
                textBoxPositionY.Text = GetFragmentsGUIValue(fragments, x => x.Data.Y);
                textBoxPositionZ.Text = GetFragmentsGUIValue(fragments, x => x.Data.Z);

                textBoxSplineT.Text = GetFragmentsGUIValue(fragments, x => x.Tension);
                textBoxSplineC.Text = GetFragmentsGUIValue(fragments, x => x.Continuity);
                textBoxSplineB.Text = GetFragmentsGUIValue(fragments, x => x.Bias);

                panelFragmentDetails.Visible = true;
            }
            else
            {
                panelFragmentDetails.Visible = false;
            }
        }

        private void UpdateHistoryGUI()
        {
            undoToolStripMenuItem.Enabled = history.Count > 0 && (historyIndex > -1 || historyIndex == null);
            redoToolStripMenuItem.Enabled = historyIndex != null && historyIndex < history.Count - 1;

            // в названия активных пунктов undo/redo добавляем названия отменяемых задач

            if (undoToolStripMenuItem.Enabled)
            {
                if (historyIndex == null)
                {
                    undoToolStripMenuItem.Text = "Undo " + history.Last().Name;
                }
                else if (historyIndex > -1)
                {
                    undoToolStripMenuItem.Text = "Undo " + history[historyIndex.Value].Name;
                }
            }
            else
            {
                undoToolStripMenuItem.Text = "Undo";
            }

            if (redoToolStripMenuItem.Enabled)
            {
                redoToolStripMenuItem.Text = "Redo " + history[historyIndex.Value + 1].Name;
            }
            else
            {
                redoToolStripMenuItem.Text = "Redo";
            }

        }

        private void UpdateMainToolBar()
        {
            addPointToolStripButton.Checked = mode == EditorMode.Creation;
            removeToolStripButton.Enabled = spline.Fragments.Any() && fragmentListBox.SelectedItems.Count > 0;
        }


        /// <summary>
        /// Преобразование координат экрана в координаты 3D-мира.
        /// </summary>
        /// <param name="mousePoint">Координаты курсора.</param>
        /// <param name="distance">Расстояние от наблюдателя до целевой точки мира.</param>
        /// <returns>Координаты точки в мире.</returns>
        /// <remarks>
        /// Координаты курсора должны быть преобразованы с учетом того, что в 3D-мире начало координат в низу экрана и Y направлена вверх.
        /// Расстояние от наблюдателя указывает, как далеко перемещается точка от плостоксти экрана внутрь мира.
        /// </remarks>
        private Vertex ScreenToWorld(Point mousePoint, float distance)
        {
            var gl = sceneControl.OpenGL;
            var nearCoords = gl.UnProject(mousePoint.X, mousePoint.Y, 0);
            var farCoords = gl.UnProject(mousePoint.X, mousePoint.Y, 0.1f);

            var nearPosition = new Vertex((float)nearCoords[0], (float)nearCoords[1], (float)nearCoords[2]);
            var farPosition = new Vertex((float)farCoords[0], (float)farCoords[1], (float)farCoords[2]);

            var eyeDirection = farPosition - nearPosition;
            eyeDirection.Normalize();

            var clickPosition = nearPosition + eyeDirection * distance;
            return clickPosition;
        }

        /// <summary>
        /// Вспомогательный метод.
        /// Возвращает строковое представление указанного параметра для всех выбранных фрагментов.
        /// </summary>
        /// <param name="fragments">Выбранные фрагменты.</param>
        /// <param name="selector">Предикат, выполняющий выборку параметра фрагмента.</param>
        /// <returns>Строковое представление значения, готовое для вывода в текстовое поле.</returns>
        /// <remarks>
        /// Метод используется для отображение координат и tcb-значений для одного или нескольких выбранных фрагментов.
        /// Если в выбранных фрагментах указанное через selector значение одинаковое, то будет выведено оно. Иначе вернутся пустая строка.
        /// </remarks>
        private string GetFragmentsGUIValue(IEnumerable<SplineFragment> fragments, Func<SplineFragment, float> selector)
        {
            var firstValue = selector(fragments.First());
            var equalValues = from fragment in fragments.Skip(1)
                              where selector(fragment) == firstValue
                              select fragments;

            if (equalValues.Count() < fragments.Count() - 1)
                return string.Empty;

            return firstValue.ToString();
        }

        /// <summary>
        /// Вспомогательный метод.
        /// Позволяет преобразовать значение текстового поля в параметр фрагмента сплайна.
        /// </summary>
        /// <param name="textBoxValue">Значение текстового поля.</param>
        /// <returns>Числовое значение текстовго поля. Либо null, если поле пустое.</returns>
        /// <remarks>
        /// Если поле пустое, значит это значение не будет изменяться.
        /// Для метода редактирования, чтобы указать поля, которые не будут меняться, нужно передавать null.
        /// </remarks>
        private float? ParseFragmentGUIValue(string textBoxValue)
        {
            if (string.IsNullOrEmpty(textBoxValue))
                return null;

            return float.Parse(textBoxValue);
        }


        private enum EditorMode
        {
            Selection,
            Creation,
            Drag
        }
    }
}
