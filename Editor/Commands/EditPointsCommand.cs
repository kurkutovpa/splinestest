﻿using SplineTest.Math;
using System.Collections.Generic;
using System.Linq;

namespace SplineTest.Editor.Commands
{
    /// <summary>
    /// Команда на изменение координат и tcb-параметров управляющей точки сплайна.
    /// </summary>
    public class EditPointsCommand : Command
    {
        private readonly SplineFragment[] fragments;
        private readonly StoredFragmentData[] oldData;
        private readonly StoredFragmentData newData;

        /// <summary>
        /// Конструктор команды.
        /// </summary>
        /// <param name="form">Ссылка ядро редактора</param>
        /// <param name="fragments">Фрагменты сплайна, которые будут подвергнуты редактированию.</param>
        /// <param name="x">Х координата управлябщей точки.</param>
        /// <param name="y">Y координата управлябщей точки.</param>
        /// <param name="z">Z координата управлябщей точки.</param>
        /// <param name="t">Tension значение управлябщей точки.</param>
        /// <param name="c">Continuity значение управлябщей точки.</param>
        /// <param name="b">Bias значение управлябщей точки.</param>
        /// <remarks>
        /// Значения, указанные как null, не будут изменяться в выбранных фрагментах.
        /// </remarks>
        public EditPointsCommand(MainForm form, IEnumerable<SplineFragment> fragments,
            float? x, float? y, float? z,
            float? t, float? c, float? b)
            : base(form)
        {
            this.fragments = fragments.ToArray();

            oldData = new StoredFragmentData[this.fragments.Length];
            for (var i = 0; i < this.fragments.Length; i++)
            {
                var fragment = this.fragments[i];
                oldData[i] = new StoredFragmentData(fragment);
            }

            newData = new StoredFragmentData(x, y, z, t, c, b);
        }

        public override void Execute()
        {
            form.EditFragment(fragments, newData.X, newData.Y, newData.Z,
                newData.Tension, newData.Continuity, newData.Bias);
        }

        public override void Revert()
        {
            for (var i = 0; i < fragments.Length; i++)
            {
                var fragment = fragments[i];
                var oldDataItem = oldData[i];
                form.EditFragment(new SplineFragment[] { fragment },
                    oldDataItem.X, oldDataItem.Y, oldDataItem.Z,
                    oldDataItem.Tension, oldDataItem.Continuity, oldDataItem.Bias);
            }

        }

        public override string Name
        {
            get { return fragments.Count() > 1 ? "Edit points" : "Edit point"; }
        }
    }
}
