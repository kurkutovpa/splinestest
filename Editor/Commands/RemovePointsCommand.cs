﻿using SplineTest.Math;
using System.Collections.Generic;
using System.Linq;

namespace SplineTest.Editor.Commands
{
    /// <summary>
    /// Команда на удаление управляющих точек сплайна.
    /// </summary>
    public class RemovePointsCommand : Command
    {
        private readonly SplineFragment[] removedFragments;

        /// <summary>
        /// Конструктор команды.
        /// </summary>
        /// <param name="form">Ссылка ядро редактора</param>
        /// <param name="fragments">Фрагменты, которые будут подвергнуты удалению.</param>
        public RemovePointsCommand(MainForm form, IEnumerable<SplineFragment> fragments)
            : base(form)
        {
            this.removedFragments = fragments.ToArray();
        }

        public override void Execute()
        {
            form.RemoveFragments(removedFragments);
        }

        public override void Revert()
        {
            foreach (var fragment in removedFragments)
                form.AddFragment(fragment);
        }

        public override string Name
        {
            get { return removedFragments.Count() > 1 ? "Remove points" : "Remove point"; }
        }
    }
}
