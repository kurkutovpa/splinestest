﻿
using SplineTest.Math;
namespace SplineTest.Editor.Commands
{
    public abstract class Command
    {
        protected readonly MainForm form;

        /// <summary>
        /// Создание экземпляра задачи. Общий конструктор.
        /// </summary>
        /// <param name="form">Ссылка ядро редактора</param>
        /// <remarks>
        /// На данный момент все команды взаимодействуют с главным окном редактора,
        /// потому что в нем сосредоточен весь функционал редактирования сплайна.
        /// </remarks>
        public Command(MainForm form)
        {
            this.form = form;
        }

        /// <summary>
        /// Выполнение команды.
        /// </summary>
        /// <remarks>
        /// Этот метод вызывается для выполнения команды, 
        /// а так же при выполнении операции redo.
        /// Потомки должны переопределить этот метод для выполнения конкретной задачи,
        /// а так же операции redo.
        /// </remarks>
        public abstract void Execute();

        /// <summary>
        /// Откат команды.
        /// </summary>
        /// <remarks>
        /// Этот метод вызывается при выполнении операции undo.
        /// Потомки должны переопределить этот метод для выполнения конкретной операции отката.
        /// </remarks>
        public abstract void Revert();

        /// <summary>
        /// Название команды, выводимое в меню.
        /// </summary>
        /// <remarks>
        /// Потомки должны переопределить данное свойство, 
        /// чтобы выводить конкретное название команды в своем контексте.
        /// </remarks>
        public abstract string Name { get; }


        /// <summary>
        /// Служебная структура для хранения изменений во фрагментах сплайна.
        /// </summary>
        protected struct StoredFragmentData
        {
            public float? X;
            public float? Y;
            public float? Z;
            public float? Tension;
            public float? Continuity;
            public float? Bias;

            public StoredFragmentData(float? x, float? y, float? z, float? t, float? c, float? b)
            {
                X = x;
                Y = y;
                Z = z;
                Tension = t;
                Continuity = c;
                Bias = b;
            }

            public StoredFragmentData(SplineFragment fragment)
                : this(fragment.Data.X, fragment.Data.Y, fragment.Data.Z,
                fragment.Tension, fragment.Continuity, fragment.Bias) { }
        }
    }
}
