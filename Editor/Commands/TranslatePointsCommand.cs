﻿using SharpGL.SceneGraph;
using SplineTest.Math;
using System.Collections.Generic;
using System.Linq;

namespace SplineTest.Editor.Commands
{
    /// <summary>
    /// Команда на перемещение управляющей точки сплайна.
    /// </summary>
    public class TranslatePointsCommand : Command
    {
        private readonly SplineFragment[] fragments;
        private readonly StoredFragmentData[] oldData;
        private readonly Vertex translation;
        private StoredFragmentData[] translatedData;
        private bool executedOnce;

        /// <summary>
        /// Конструктор команды.
        /// </summary>
        /// <param name="form">Ссылка ядро редактора</param>
        /// <param name="fragments">Фрагменты сплайна, которые будут подвергнуты перемещению.</param>
        /// <param name="translation">Вектор, на который будут перемещены управляющие точки.</param>
        public TranslatePointsCommand(MainForm form, IEnumerable<SplineFragment> fragments,
            Vertex translation)
            : base(form)
        {
            this.fragments = fragments.ToArray();
            this.translation = translation;

            oldData = new StoredFragmentData[this.fragments.Length];
            for (var i = 0; i < this.fragments.Length; i++)
            {
                var fragment = this.fragments[i];
                oldData[i] = new StoredFragmentData(fragment);
            }
        }

        public override void Execute()
        {
            if (!executedOnce)
            {
                form.TranslateFragments(fragments, translation);

                translatedData = new StoredFragmentData[this.fragments.Length];
                for (var i = 0; i < this.fragments.Length; i++)
                {
                    var fragment = this.fragments[i];
                    translatedData[i] = new StoredFragmentData(fragment);
                }

                executedOnce = true;
            }
            else
            {
                for (var i = 0; i < fragments.Length; i++)
                {
                    var fragment = fragments[i];
                    var translatedDataItem = translatedData[i];
                    form.EditFragment(new SplineFragment[] { fragment },
                        translatedDataItem.X, translatedDataItem.Y, translatedDataItem.Z,
                        null, null, null);
                }
            }
        }

        public override void Revert()
        {
            for (var i = 0; i < fragments.Length; i++)
            {
                var fragment = fragments[i];
                var oldDataItem = oldData[i];
                form.EditFragment(new SplineFragment[] { fragment },
                    oldDataItem.X, oldDataItem.Y, oldDataItem.Z,
                    null, null, null);
            }

        }

        public override string Name
        {
            get { return fragments.Count() > 1 ? "Translate points" : "Translate point"; }
        }
    }
}
