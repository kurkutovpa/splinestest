﻿using SharpGL.SceneGraph;
using SplineTest.Math;

namespace SplineTest.Editor.Commands
{
    /// <summary>
    /// Команда на добавление управляющей точки сплайна.
    /// </summary>
    public class AddPointCommand : Command
    {
        private readonly Vertex position;

        private SplineFragment newFragment;

        /// <summary>
        /// Конструктор команды.
        /// </summary>
        /// <param name="form">Ссылка ядро редактора</param>
        /// <param name="position">Координаты, в которые будет добавлена управляющая точка.</param>
        public AddPointCommand(MainForm form, Vertex position)
            : base(form)
        {
            this.position = position;
        }

        public override void Execute()
        {
            this.newFragment = form.AddFragment(position);
        }

        public override void Revert()
        {
            form.RemoveFragments(new SplineFragment[] { newFragment });
        }

        public override string Name
        {
            get { return "Add point"; }
        }
    }
}
