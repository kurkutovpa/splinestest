﻿using System.Collections.Generic;

namespace SplineTest.Math
{
    /// <summary>
    /// Статический класс, выполняющий интерполяцию данных TCB-сплайнами по указанным фрагментам.
    /// </summary>
    public static class TCBSplines
    {
        /// <summary>
        /// Выполнение интерполяции.
        /// </summary>
        /// <param name="source">Набор фрагментов.</param>
        /// <remarks>
        /// В source передаются все фрагменты сплайна в хронологическом порядке.
        /// Расчет интерполированных значений будет производиться только для тех фрагментов,
        /// для которых Points равен null.
        /// </remarks>
        public static void Interpolate(SplineFragment[] source)
        {
            if (source.Length <= 1)
                return;

            if (source.Length == 2)
            {
                LinearInterpolate(source);
                return;
            }

            var interpolatedPoints = new List<Vector3D>(100);

            for (var i = 0; i < source.Length - 1; i++)
            {
                if (source[i].Points != null)
                    continue;

                var prev = i > 0 ? source[i - 1] : source[i];
                var cur = source[i];
                var next = i < source.Length - 1 ? source[i + 1] : source[i];
                var next2 = i < source.Length - 2 ? source[i + 2] : source[i];

                // расчитываем производные в начале и в конце локального времени
                var r1 = 0.5f * (1 - cur.Tension) * ((1 + cur.Bias) * (1 - cur.Continuity) * (cur.Data - prev.Data) + (1 - cur.Bias) * (1 + cur.Continuity) * (next.Data - cur.Data));
                var r2 = 0.5f * (1 - next.Tension) * ((1 + next.Bias) * (1 + next.Continuity) * (next.Data - cur.Data) + (1 - next.Bias) * (1 - next.Continuity) * (next2.Data - next.Data));

                // через равные промежутки рассчитываем значения функции
                // и записываем результаты вычислений для данного локального времени
                interpolatedPoints.Clear();
                for (var t = 0.0f; t <= 1.0f; t += 0.01f)
                {
                    var res = f(t, cur.Data, next.Data, r1, r2);

                    interpolatedPoints.Add(new Vector3D
                    {
                        X = res.X,
                        Y = res.Y,
                        Z = res.Z
                    });
                }
                source[i].Points = interpolatedPoints.ToArray();
            }
        }

        /// <summary>
        /// Вспомогательный метод. Выполняет кусочно-линейную интерполяцию. Используется, когда нет смысла рассчитывать сплайны.
        /// </summary>
        /// <param name="source">Набор фрагментов.</param>
        private static void LinearInterpolate(SplineFragment[] source)
        {
            foreach (var fragment in source)
            {
                fragment.Points = new Vector3D[] { fragment.Data };
            }
        }

        /// <summary>
        /// Вспомогательный метод. Является функцией кубической параболы через указанные точки с учетом производных в этих точках.
        /// </summary>
        /// <param name="t">Локальное время. Принимает значения в диапазоне [0..1)</param>
        /// <param name="p1">Координаты "начала" параболы</param>
        /// <param name="p2">Координаты "конца" параболы</param>
        /// <param name="r1">Производная в точке p1</param>
        /// <param name="r2">Производная в точке p2</param>
        /// <returns>Значение параболы во время t.</returns>
        private static Vector3D f(float t, Vector3D p1, Vector3D p2, Vector3D r1, Vector3D r2)
        {
            var tP2 = (float)System.Math.Pow(t, 2);
            var tP3 = (float)System.Math.Pow(t, 3);
            return p1 * (2 * tP3 - 3 * tP2 + 1) +
                r1 * (tP3 - 2 * tP2 + t) +
                p2 * (-2 * tP3 + 3 * tP2) +
                r2 * (tP3 - tP2);
        }
    }
}
