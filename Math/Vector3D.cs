﻿
namespace SplineTest.Math
{
    /// <summary>
    /// Вектор в трехмерном пространстве.
    /// </summary>
    [System.Serializable]
    public struct Vector3D
    {
        public float X;
        public float Y;
        public float Z;

        public Vector3D(float x, float y, float z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public static Vector3D operator +(Vector3D a, Vector3D b)
        {
            return new Vector3D
            {
                X = a.X + b.X,
                Y = a.Y + b.Y,
                Z = a.Z + b.Z
            };
        }

        public static Vector3D operator -(Vector3D a, Vector3D b)
        {
            return a + (-1) * b;
        }

        public static Vector3D operator *(Vector3D a, float b)
        {
            return new Vector3D
            {
                X = a.X * b,
                Y = a.Y * b,
                Z = a.Z * b
            };
        }

        public static Vector3D operator *(float b, Vector3D a)
        {
            return a * b;
        }
    }
}
