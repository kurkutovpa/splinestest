﻿
namespace SplineTest.Math
{
    /// <summary>
    /// Фрагмент данных.
    /// </summary>
    /// <remarks>
    /// Представляет собой совокупность всех необходимых данных для представления координат и 
    /// tcb-параметров управляющей точки, а так же набора интерполированных данных.
    /// </remarks>
    [System.Serializable]
    public class SplineFragment
    {
        /// <summary>
        /// Координаты управляющей точки в мире.
        /// </summary>
        /// <remarks>
        /// В общем случаем может представлять сколько угодно компонентный набор характеристик объекта.
        /// </remarks>
        public Vector3D Data { get; set; }

        /// <summary>
        /// Tension-характеристика управляющей точки.
        /// </summary>
        public float Tension { get; set; }

        /// <summary>
        /// Continuity-характеристика управляющей точки.
        /// </summary>
        public float Continuity { get; set; }

        /// <summary>
        /// Bias-характеристика управляющей точки.
        /// </summary>
        public float Bias { get; set; }

        /// <summary>
        /// Набор интерполированных данных между текущей управляющей точкой и следующей.
        /// </summary>
        /// <remarks>
        /// Последний фрагмент сплайна всегда имеет это значение, равное null.
        /// Потому что за ним не следует управляющих точек и нечего интерполировать.
        /// </remarks>
        public Vector3D[] Points { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", Data.X, Data.Y, Data.Z);
        }
    }
}
