﻿using System.Collections.Generic;

namespace SplineTest.Math
{
    /// <summary>
    /// Сплайн.
    /// </summary>
    /// <remarks>
    /// Сплайн представляет собой совокупность <a cref="SplineFragment">фрагментов</a>  и выполняет необходимые операции
    /// </remarks>
    public class Spline
    {
        /// <summary>
        /// Список всех фрагментов.
        /// </summary>
        public readonly List<SplineFragment> Fragments = new List<SplineFragment>();

        /// <summary>
        /// Пересчитывает интерполированные значения для всех фрагментов данных.
        /// </summary>
        /// <param name="eriseAll">Указывает, нуобходимо ли очистить интерполированные данные всех фрагментов сплайна.</param>
        /// <remarks>
        /// Интерполяция данных производится только для тех фрагментов, для которых нет интерполированных данных.
        /// Данный метод либо пересчитывает сплайны для таких фрагментов. Либо выполняет пересчет для всех фрагментов сплайна.
        /// </remarks>
        public void Recalculate(bool eriseAll = false)
        {
            foreach (var fragment in Fragments)
                fragment.Points = null;

            TCBSplines.Interpolate(Fragments.ToArray());
        }

        /// <summary>
        /// Пересчитывает значения для указанного фрагмента данных.
        /// </summary>
        /// <param name="index">Индекс фрагменто в коллекции сплайна.</param>
        public void RecalculatePoint(int index)
        {
            EriseInterpolation(index);
            TCBSplines.Interpolate(Fragments.ToArray());
        }

        /// <summary>
        /// Очищает интерполированные значения для указанного фрагмента.
        /// </summary>
        /// <param name="fragment">Фрагмент, для которого интерполированные данные стали неактуальны.</param>
        /// <remarks>
        /// Отличается от RecalculatePoint тем, что не выполняет операцию пересчета интерполированных значений.
        /// </remarks>
        public void EriseInterpolation(SplineFragment fragment)
        {
            var index = Fragments.IndexOf(fragment);
            EriseInterpolation(index);
        }

        /// <summary>
        /// Очищает интерполированные значения для указанного фрагмента.
        /// </summary>
        /// <param name="index">Индекс фрагмента, для которого интерполированные данные стали неактуальны.</param>
        /// <remarks>
        /// Отличается от RecalculatePoint тем, что не выполняет операцию пересчета интерполированных значений.
        /// </remarks>
        public void EriseInterpolation(int index)
        {
            // выбираем по минус два элемента в каждую сторону.
            // При этом соблюдаем, чтобы индексы не выходили за границы массива.
            var lowIndex = System.Math.Max(index - 2, 0);
            var highIndex = System.Math.Min(index + 2, Fragments.Count - 1);

            // обнуляем интерполированные данные
            for (var i = lowIndex; i <= highIndex; i++)
                Fragments[i].Points = null;
        }
    }
}
